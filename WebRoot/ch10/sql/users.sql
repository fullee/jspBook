/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2015-11-23 14:29:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `userId` varchar(16) DEFAULT NULL,
  `password` varchar(33) DEFAULT NULL,
  `type` bit(1) DEFAULT NULL,
  `cityId` int(10) DEFAULT NULL,
  `ucollegeId` int(10) DEFAULT NULL,
  `score` int(10) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tel` varchar(15) DEFAULT NULL,
  `headPhoto` varchar(36) DEFAULT NULL,
  `realName` varchar(20) DEFAULT NULL,
  `gender` bit(1) DEFAULT NULL,
  `address` varchar(80) DEFAULT NULL,
  `postalcode` varchar(10) DEFAULT NULL,
  `question` varchar(50) DEFAULT NULL,
  `answer` varchar(50) DEFAULT NULL,
  `qq` varchar(50) DEFAULT NULL,
  `msn` varchar(50) DEFAULT NULL,
  `taobaoId` varchar(20) DEFAULT NULL,
  `intro` text,
  `regDate` datetime(6) DEFAULT NULL,
  `lastLoginTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `loginCounts` int(10) DEFAULT NULL,
  `statusCode` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
