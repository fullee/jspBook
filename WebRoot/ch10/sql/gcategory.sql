/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : 15eu

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2015-11-23 10:02:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for gcategory
-- ----------------------------
DROP TABLE IF EXISTS `gcategory`;
CREATE TABLE `gcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gcname` varchar(60) DEFAULT NULL,
  `gcparentId` int(11) DEFAULT NULL,
  `gcorder` smallint(3) DEFAULT NULL,
  `gctopId` int(11) DEFAULT NULL,
  `gclevel` smallint(6) DEFAULT NULL,
  `leafnode` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=635 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gcategory
-- ----------------------------
INSERT INTO `gcategory` VALUES ('10', '家居日用家具文具', '0', '1', '10', '0', '\0');
INSERT INTO `gcategory` VALUES ('11', '家居日用', '10', '1', '10', '1', '\0');
INSERT INTO `gcategory` VALUES ('12', '家具家饰', '10', '2', '10', '1', '\0');
INSERT INTO `gcategory` VALUES ('13', '食品饮品及器具', '10', '3', '10', '1', '\0');
INSERT INTO `gcategory` VALUES ('20', '运动休闲书籍玩具', '0', '2', '20', '0', '\0');
INSERT INTO `gcategory` VALUES ('21', '运动/户外/保健', '20', '1', '20', '1', '\0');
INSERT INTO `gcategory` VALUES ('22', '玩具/卡通/模型', '20', '2', '20', '1', '\0');
INSERT INTO `gcategory` VALUES ('23', '书籍/杂志/报纸', '20', '3', '20', '1', '\0');
INSERT INTO `gcategory` VALUES ('24', '音乐/影视/明星/娱乐', '20', '2', '20', '1', '\0');
INSERT INTO `gcategory` VALUES ('25', '宠物及用品', '20', '5', '20', '1', '\0');
INSERT INTO `gcategory` VALUES ('30', '男女服饰婴孕用品', '0', '3', '30', '0', '\0');
INSERT INTO `gcategory` VALUES ('31', '女装/女士用品', '30', '1', '30', '1', '\0');
INSERT INTO `gcategory` VALUES ('32', '男装/男士用品', '30', '2', '30', '1', '\0');
INSERT INTO `gcategory` VALUES ('33', '童装/婴幼/孕妇用品', '30', '3', '30', '1', '\0');
INSERT INTO `gcategory` VALUES ('40', '家电数码电脑通讯', '0', '4', '40', '0', '\0');
INSERT INTO `gcategory` VALUES ('41', '数码产品/摄像机/随身视听', '40', '1', '40', '1', '\0');
INSERT INTO `gcategory` VALUES ('42', '电脑/软件/网络/办公设备', '40', '2', '40', '1', '\0');
INSERT INTO `gcategory` VALUES ('43', '家用电器', '40', '3', '40', '1', '\0');
INSERT INTO `gcategory` VALUES ('44', '通讯工具/器材', '40', '4', '40', '1', '\0');
INSERT INTO `gcategory` VALUES ('45', '网络虚拟商品', '40', '5', '40', '1', '\0');
INSERT INTO `gcategory` VALUES ('50', '交通工具', '0', '5', '50', '0', '\0');
INSERT INTO `gcategory` VALUES ('51', '自行车/电动车', '50', '1', '50', '1', '');
INSERT INTO `gcategory` VALUES ('52', '汽车/摩托车', '50', '2', '50', '1', '');
INSERT INTO `gcategory` VALUES ('53', '汽摩电器/视听设备', '50', '3', '50', '1', '');
INSERT INTO `gcategory` VALUES ('54', '汽车饰品/保养用品', '50', '4', '50', '1', '');
INSERT INTO `gcategory` VALUES ('55', '车载通讯/GPS', '50', '5', '50', '1', '');
INSERT INTO `gcategory` VALUES ('56', '各种车配件', '50', '6', '50', '1', '');
INSERT INTO `gcategory` VALUES ('57', '助力车/其他', '50', '7', '50', '1', '');
INSERT INTO `gcategory` VALUES ('60', '古董票卡表镜饰品', '0', '6', '60', '0', '\0');
INSERT INTO `gcategory` VALUES ('61', '邮币/古董/字画/收藏', '60', '2', '60', '1', '\0');
INSERT INTO `gcategory` VALUES ('62', '珠宝/首饰/手表/眼镜', '60', '2', '60', '1', '\0');
INSERT INTO `gcategory` VALUES ('63', '票证/卡类', '60', '3', '60', '1', '\0');
INSERT INTO `gcategory` VALUES ('70', '房产家装', '0', '7', '70', '0', '\0');
INSERT INTO `gcategory` VALUES ('71', '住宅/商铺/写字间', '70', '1', '70', '1', '');
INSERT INTO `gcategory` VALUES ('72', '灯饰灯具', '70', '2', '70', '1', '');
INSERT INTO `gcategory` VALUES ('73', '家装饰材', '70', '3', '70', '1', '');
INSERT INTO `gcategory` VALUES ('74', '门窗/玻璃', '70', '4', '70', '1', '');
INSERT INTO `gcategory` VALUES ('75', '中央空调', '70', '5', '70', '1', '');
INSERT INTO `gcategory` VALUES ('76', '卫浴洁具/其他', '70', '6', '70', '1', '');
INSERT INTO `gcategory` VALUES ('80', '农林牧渔行业', '0', '8', '80', '0', '\0');
INSERT INTO `gcategory` VALUES ('90', '工矿建筑行业', '0', '9', '90', '0', '\0');
INSERT INTO `gcategory` VALUES ('91', '轻纺食品加工', '90', '1', '90', '1', '');
INSERT INTO `gcategory` VALUES ('92', '化工医药医疗', '90', '2', '90', '1', '');
INSERT INTO `gcategory` VALUES ('93', '电子信息通信产业', '90', '3', '90', '1', '');
INSERT INTO `gcategory` VALUES ('94', '机电机械业', '90', '4', '90', '1', '');
INSERT INTO `gcategory` VALUES ('95', '汽车行业', '90', '5', '90', '1', '');
INSERT INTO `gcategory` VALUES ('96', '矿山冶金行业', '90', '6', '90', '1', '');
INSERT INTO `gcategory` VALUES ('97', '建筑设备/建材', '90', '7', '90', '1', '');
INSERT INTO `gcategory` VALUES ('100', '餐饮商业服务行业', '0', '10', '100', '0', '\0');
INSERT INTO `gcategory` VALUES ('111', '生活日用', '11', '1', '10', '2', '');
INSERT INTO `gcategory` VALUES ('112', '洗浴/卫浴用品', '11', '2', '10', '2', '');
INSERT INTO `gcategory` VALUES ('113', '电池/电源/家用电力设备', '11', '3', '10', '2', '');
INSERT INTO `gcategory` VALUES ('114', '文具/办公用品', '11', '4', '10', '2', '');
INSERT INTO `gcategory` VALUES ('115', '日用五金工具/其他', '11', '5', '10', '2', '');
INSERT INTO `gcategory` VALUES ('121', '木质/铁艺等家具', '12', '1', '10', '2', '');
INSERT INTO `gcategory` VALUES ('122', '时尚家饰', '12', '2', '10', '2', '');
INSERT INTO `gcategory` VALUES ('123', '布艺/床品/靠垫/窗帘', '12', '3', '10', '2', '');
INSERT INTO `gcategory` VALUES ('124', '鲜花园艺/其他', '12', '4', '10', '2', '');
INSERT INTO `gcategory` VALUES ('131', '食品/保健食品', '13', '1', '10', '2', '');
INSERT INTO `gcategory` VALUES ('132', '茶叶/烟酒/咖啡', '13', '2', '10', '2', '');
INSERT INTO `gcategory` VALUES ('133', '厨房用具/餐饮器具', '13', '3', '10', '2', '');
INSERT INTO `gcategory` VALUES ('134', '土特食品/其他', '13', '4', '10', '2', '');
INSERT INTO `gcategory` VALUES ('211', '运动/体育用品', '21', '1', '20', '2', '');
INSERT INTO `gcategory` VALUES ('212', '户外/休闲', '21', '2', '20', '2', '');
INSERT INTO `gcategory` VALUES ('213', '保健/健身', '21', '3', '20', '2', '');
INSERT INTO `gcategory` VALUES ('214', '乐器/其他', '21', '4', '20', '2', '');
INSERT INTO `gcategory` VALUES ('221', '益智/毛绒', '22', '1', '20', '2', '');
INSERT INTO `gcategory` VALUES ('222', '卡通/动漫', '22', '2', '20', '2', '');
INSERT INTO `gcategory` VALUES ('223', '风筝/航模', '22', '3', '20', '2', '');
INSERT INTO `gcategory` VALUES ('224', '手柄/其他', '22', '4', '20', '2', '');
INSERT INTO `gcategory` VALUES ('231', '文学/艺术', '23', '1', '20', '2', '');
INSERT INTO `gcategory` VALUES ('232', '科技/计算机', '23', '2', '20', '2', '');
INSERT INTO `gcategory` VALUES ('233', '社科/经济管', '23', '3', '20', '2', '');
INSERT INTO `gcategory` VALUES ('234', '工具书/辞书', '23', '4', '20', '2', '');
INSERT INTO `gcategory` VALUES ('235', '漫画/杂志', '23', '5', '20', '2', '');
INSERT INTO `gcategory` VALUES ('236', '外语/考试/教材', '23', '6', '20', '2', '');
INSERT INTO `gcategory` VALUES ('237', '报纸/贺卡/年历/其他', '23', '7', '20', '2', '');
INSERT INTO `gcategory` VALUES ('241', '音乐/戏曲/曲艺唱片', '24', '1', '20', '2', '');
INSERT INTO `gcategory` VALUES ('242', '影视VCD/DVD', '24', '2', '20', '2', '');
INSERT INTO `gcategory` VALUES ('243', '娱乐/演唱会门票', '24', '3', '20', '2', '');
INSERT INTO `gcategory` VALUES ('244', '明星纪念品/其他', '24', '4', '20', '2', '');
INSERT INTO `gcategory` VALUES ('251', '猫狗等宠物', '25', '1', '20', '2', '');
INSERT INTO `gcategory` VALUES ('252', '宠物食品及用品', '25', '2', '20', '2', '');
INSERT INTO `gcategory` VALUES ('253', '宠物保健/美容/服饰', '25', '3', '20', '2', '');
INSERT INTO `gcategory` VALUES ('254', '宠物玩具及其他', '25', '4', '20', '2', '');
INSERT INTO `gcategory` VALUES ('311', '衣/裤/裙', '31', '1', '30', '2', '');
INSERT INTO `gcategory` VALUES ('312', '鞋/帽/包', '31', '2', '30', '2', '');
INSERT INTO `gcategory` VALUES ('313', '外套/套装/风衣', '31', '3', '30', '2', '');
INSERT INTO `gcategory` VALUES ('314', '内衣丝袜', '31', '4', '30', '2', '');
INSERT INTO `gcategory` VALUES ('315', '女士配件', '31', '5', '30', '2', '');
INSERT INTO `gcategory` VALUES ('316', '彩妆/香水/化妆品', '31', '6', '30', '2', '');
INSERT INTO `gcategory` VALUES ('317', '护肤/美体/其他', '31', '7', '30', '2', '');
INSERT INTO `gcategory` VALUES ('321', '衬衫/毛衣/针织衫/T恤', '32', '1', '30', '2', '');
INSERT INTO `gcategory` VALUES ('322', '男士内衣裤/西裤', '32', '2', '30', '2', '');
INSERT INTO `gcategory` VALUES ('323', '牛仔系列', '32', '3', '30', '2', '');
INSERT INTO `gcategory` VALUES ('324', '外套/套装/风衣/西装', '32', '4', '30', '2', '');
INSERT INTO `gcategory` VALUES ('325', '鞋/帽/包', '32', '5', '30', '2', '');
INSERT INTO `gcategory` VALUES ('326', '剃须刀/刮胡刀', '32', '6', '30', '2', '');
INSERT INTO `gcategory` VALUES ('327', '领带/皮带/服饰配件', '32', '7', '30', '2', '');
INSERT INTO `gcategory` VALUES ('328', '火机/烟具/其他', '32', '8', '30', '2', '');
INSERT INTO `gcategory` VALUES ('331', '婴儿/儿童服装及配件', '33', '1', '30', '2', '');
INSERT INTO `gcategory` VALUES ('332', '童鞋/婴儿鞋帽', '33', '2', '30', '2', '');
INSERT INTO `gcategory` VALUES ('333', '孕前及孕期用品', '33', '3', '30', '2', '');
INSERT INTO `gcategory` VALUES ('334', '童车/宝宝车', '33', '4', '30', '2', '');
INSERT INTO `gcategory` VALUES ('335', '奶粉/辅食/其他', '33', '5', '30', '2', '');
INSERT INTO `gcategory` VALUES ('411', '数码/摄影/摄像机', '41', '1', '40', '2', '');
INSERT INTO `gcategory` VALUES ('412', 'MP3/MP4/CD/随身听', '41', '2', '40', '2', '');
INSERT INTO `gcategory` VALUES ('413', 'HIFI器材/音响设备', '41', '3', '40', '2', '');
INSERT INTO `gcategory` VALUES ('414', '耳机/耳麦', '41', '4', '40', '2', '');
INSERT INTO `gcategory` VALUES ('415', 'U盘/移动硬盘/闪存卡/其他', '41', '5', '40', '2', '');
INSERT INTO `gcategory` VALUES ('416', 'iPod系列产品', '41', '6', '40', '2', '');
INSERT INTO `gcategory` VALUES ('421', '电脑硬件/网络设备', '42', '1', '40', '2', '');
INSERT INTO `gcategory` VALUES ('422', '台式PC/笔记本电脑', '42', '2', '40', '2', '');
INSERT INTO `gcategory` VALUES ('423', '电子辞典/学习机/复读机', '42', '3', '40', '2', '');
INSERT INTO `gcategory` VALUES ('424', '电脑软件', '42', '4', '40', '2', '');
INSERT INTO `gcategory` VALUES ('425', '办公设备/耗材', '42', '5', '40', '2', '');
INSERT INTO `gcategory` VALUES ('426', '打印/冲印设备', '42', '6', '40', '2', '');
INSERT INTO `gcategory` VALUES ('427', '音箱/其他', '42', '7', '40', '2', '');
INSERT INTO `gcategory` VALUES ('431', '小家电', '43', '1', '40', '2', '');
INSERT INTO `gcategory` VALUES ('432', '电视/影碟机/功放', '43', '2', '40', '2', '');
INSERT INTO `gcategory` VALUES ('433', '洗衣机/冰箱/空调', '43', '3', '40', '2', '');
INSERT INTO `gcategory` VALUES ('434', '风扇/其他', '43', '4', '40', '2', '');
INSERT INTO `gcategory` VALUES ('441', '手机/电话/小灵通', '44', '1', '40', '2', '');
INSERT INTO `gcategory` VALUES ('442', '手机配件', '44', '2', '40', '2', '');
INSERT INTO `gcategory` VALUES ('443', '手机饰品/其他', '44', '3', '40', '2', '');
INSERT INTO `gcategory` VALUES ('451', 'QQ号码/QQ群/网游账号', '45', '1', '40', '2', '');
INSERT INTO `gcategory` VALUES ('452', '网游金币/点卡/装备', '45', '2', '40', '2', '');
INSERT INTO `gcategory` VALUES ('453', '域名/站点代码/其他', '45', '3', '40', '2', '');
INSERT INTO `gcategory` VALUES ('611', '邮品/邮币/钱币', '61', '1', '60', '2', '');
INSERT INTO `gcategory` VALUES ('612', '字画/古玩/古董', '61', '2', '60', '2', '');
INSERT INTO `gcategory` VALUES ('613', '烟花/趣味收集', '61', '3', '60', '2', '');
INSERT INTO `gcategory` VALUES ('614', '徽章/收藏品/其他', '61', '4', '60', '2', '');
INSERT INTO `gcategory` VALUES ('621', '珠宝首饰', '62', '1', '60', '2', '');
INSERT INTO `gcategory` VALUES ('622', '民族饰品', '62', '2', '60', '2', '');
INSERT INTO `gcategory` VALUES ('623', '挂件/吊饰/摆件', '62', '3', '60', '2', '');
INSERT INTO `gcategory` VALUES ('624', '手表/表带', '62', '4', '60', '2', '');
INSERT INTO `gcategory` VALUES ('625', '眼镜/其他', '62', '5', '60', '2', '');
INSERT INTO `gcategory` VALUES ('631', '车票/机票/船票/交通卡', '63', '1', '60', '2', '');
INSERT INTO `gcategory` VALUES ('632', '电话/充值/上网/游戏卡', '63', '2', '60', '2', '');
INSERT INTO `gcategory` VALUES ('633', '餐饮/住宿/超市会员卡', '63', '3', '60', '2', '');
INSERT INTO `gcategory` VALUES ('634', '购物卡/其他', '63', '4', '60', '2', '');
