<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'gcList.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  <h1>易物网易品分类管理</h1>
   	<%
   		Connection  conn = null;
   		Statement stat = null;
   		ResultSet list = null;
   		
  		  try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=123456&useUnicode=true&characterEncoding=UTF8");
			stat = conn.createStatement();
			list =stat.executeQuery("select * from gcategory");
		
   	 %>
   	 <table>
   	 
   	 	<tr>
   	 		<th>序号</th>
   	 		<th>类别ID</th>
   	 		<th>类别名称</th>
   	 		<th>上级分类ID</th>
   	 		<th>分类排序</th>
   	 		<th>顶级分类ID</th>
   	 		<th>分类级别</th>
   	 		<th>是否末端</th>
   	 	</tr>
   	 	<%
	   	 	int i = 0;
	   	 	while(list.next()){
	   	 		%>
	   	 		<tr>
	   	 			<td><%=i++ %></td>
	   	 			<td><%=list.getInt("id") %></td>
	   	 			<td><%=list.getString("gcname") %></td>
	   	 			<td><%=list.getInt("gcparentId") %></td>
	   	 			<td><%=list.getInt("gcorder") %></td>
	   	 			<td><%=list.getInt("gctopId") %></td>
	   	 			<td><%=list.getInt("gclevel") %></td>
	   	 			<td><%=list.getInt("leafnode") %></td>
	   	 		</tr>
	   	 		<%
	   	 	}
   	 	} catch (Exception e) {
			e.printStackTrace();
		}finally{
			list.close();
			stat.close();
			conn.close();
		}	
   	 	 %>
   	 </table>
  </body>
</html>
